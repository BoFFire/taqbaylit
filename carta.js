var car;
function transcrire() {
car = document.conversion.saisie.value;
car = car.replace(/_/g, "=");
car = car.replace(/c=/g, "č");
car = car.replace(/d=/g, "ḍ");
car = car.replace(/g=/g, "ǧ");
car = car.replace(/h=/g, "ḥ");
car = car.replace(/r=/g, "ṛ");
car = car.replace(/s=/g, "ṣ");
car = car.replace(/t=/g, "ṭ");
car = car.replace(/z=/g, "ẓ");
car = car.replace(/C=/g, "Č");
car = car.replace(/D=/g, "Ḍ");
car = car.replace(/g=/g, "Ǧ");
car = car.replace(/H=/g, "Ḥ");
car = car.replace(/R=/g, "Ṛ");
car = car.replace(/S=/g, "Ṣ");
car = car.replace(/T=/g, "Ṭ");
car = car.replace(/Z=/g, "Ẓ");
car = car.replace(/e=/g, "ɛ");
car = car.replace(/E=/g, "Ɛ");
car = car.replace(/y=/g, "γ");
car = car.replace(/Y=/g, "Γ");
// grec
car = car.replace(/a=/g, "ɛ");
car = car.replace(/â/g, "ɛ");
car = car.replace(/v/g, "γ");
car = car.replace(/V/g, "Γ");
document.conversion.saisie.value=car;
var obj=document.conversion.saisie;
obj.focus();
obj.scrollTop=obj.scrollHeight;
}
